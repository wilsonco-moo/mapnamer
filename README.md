# Map naming program

This program is a simple demonstration of RascUI. It allows many people to
collaborate, and think of names. This is helpful for the development of
[Rasc](https://gitlab.com/wilsonco-moo/rasc) maps.

![Image](screenshots/connectWindow.png)
![Image](screenshots/mainWindow.png)
