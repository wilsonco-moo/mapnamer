#include <rascUItheme/DynamicThemeLoader.h>
#include <rascUI/platform/WindowConfig.h>
#include <sockets/plus/misc.h>
#include <iostream>
#include <cstddef>
#include <cstdlib>
#include <string>

#ifdef _WIN32
    #include <rascUIwin/platform/Window.h>
    #include <rascUIwin/platform/Context.h>
    namespace platform = rascUIwin;
    #define THEME_TO_USE "scalableTheme.dll"
    #ifdef FORCE_ENABLE_WIN_SCALABLE_THEME
        #include <rascUItheme/themes/win/scalableTheme/ScalableTheme.h>
    #endif
    #ifdef FORCE_ENABLE_WIN_BASIC_THEME
        #include <rascUItheme/themes/win/basicTheme/BasicTheme.h>
    #endif
#else
    #include <rascUIxcb/platform/Window.h>
    #include <rascUIxcb/platform/Context.h>
    namespace platform = rascUIxcb;
    #define THEME_TO_USE "libscalableTheme.so"
#endif

#include "mapnamer/ConnectWindow.h"
#include "mapnamer/MainWindow.h"
#include "mapnamer/Server.h"

int main(int argc, char ** argv) {
    simplenetwork::socketsInit();
    
    if (argc >= 2 && std::string(argv[1]) == "server") {
        std::string portStr;
        std::cout << "Enter server port, or nothing for default (64000): ";
        std::getline(std::cin, portStr);
        uint16_t port;
        if (portStr.empty()) {
            port = 64000;
        } else {
            port = (uint16_t)std::strtoul(portStr.c_str(), NULL, 0);
        }
        mapnamer::Server server;
        if (server.listenToPort(port)) {
            std::cout << "Server started successfully, on port " << port << ".\n";
            std::cout << "Press ENTER to close server.\n";
            std::string str;
            std::getline(std::cin, str);
        } else {
            std::cout << "Failed to start server.\n";
        }
    
    } else {
        // The UI scale, and the exponent version.
        GLfloat scale = 1.0f;
        int scaleExp = 0;
        
        // Load theme (using static version if needed for windows.
        #ifdef FORCE_ENABLE_WIN_SCALABLE_THEME
            winScalableTheme::ScalableTheme scalableTheme;
            rascUI::Theme * theme = &scalableTheme;
        #else
            #ifdef FORCE_ENABLE_WIN_BASIC_THEME
                winBasicTheme::BasicTheme basicTheme;
                rascUI::Theme * theme = &basicTheme;
            #else
                rascUItheme::DynamicThemeLoader themeLoader((argc >= 2) ? argv[1] : THEME_TO_USE);
                if (!themeLoader.loadedSuccessfully()) {
                    std::cerr << "Failed to load theme " << ((argc >= 2) ? argv[1] : THEME_TO_USE) << "\n";
                    return EXIT_FAILURE;
                }
                rascUI::Theme * theme = themeLoader.getTheme();
            #endif
        #endif
        
        // Create the context.
        platform::Context context(theme, &scale, &scale);
        if (!context.wasSuccessful()) {
            std::cerr << "Failed to initialise context.\n";
            return EXIT_FAILURE;
        }
        
        // Create main window (which contains the network interface), but don't show it yet.
        rascUI::WindowConfig config;
        config.title = "Please submit names";
        config.screenWidth = 640;
        config.screenHeight = 480;
        mapnamer::MainWindow mainWindow(&context, config, theme, &scale, &scaleExp);
        
        // Run connect window.
        {
            config.title = "Connect to a server";
            config.screenWidth = 320;
            config.screenHeight = 200;
            mapnamer::ConnectWindow connectWindow(&context, config, mainWindow.getClientIntf(), theme, &scale, &scaleExp);
            connectWindow.setMapped(true);
            context.mainLoop();
        }
        
        // If connection was successful, run main window.
        if (mainWindow.getClientIntf()->isRunning()) {
            mainWindow.setMapped(true);
            context.mainLoop();
        }
    }
    
    simplenetwork::socketsQuit();
    return EXIT_SUCCESS;
}
