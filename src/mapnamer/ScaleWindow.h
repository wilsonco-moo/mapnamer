/*
 * ScaleWindow.h
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#ifndef MAPNAMER_SCALEWINDOW_H_
#define MAPNAMER_SCALEWINDOW_H_

#ifdef _WIN32
    #include <rascUIwin/platform/Window.h>
    #include <rascUIwin/platform/Context.h>
    namespace platform = rascUIwin;
#else
    #include <rascUIxcb/platform/Window.h>
    #include <rascUIxcb/platform/Context.h>
    namespace platform = rascUIxcb;
#endif

namespace mapnamer {

    /**
     *
     */
    class ScaleWindow : public platform::Window {
    private:
        GLfloat * scale;
        int * scaleExp;
    
    public:
        ScaleWindow(platform::Context * context, const rascUI::WindowConfig & config, GLfloat * scale, int * scaleExp);
        virtual ~ScaleWindow(void);
        
        virtual void onUnusedKeyPress(int key, bool special) override;
    };
}

#endif
