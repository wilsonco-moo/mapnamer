/*
 * MainWindow.cpp
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#include "MainWindow.h"

#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "ClientIntf.h"
#include "Server.h"

#define LAYOUT_THEME theme



namespace mapnamer {
    
    // Our container is fitted into this.
    #define ENTRY_OVERALL_LOC \
        SUB_BORDER_X(SUB_BORDER_B(GEN_FILL))
    
    // The ID panel/label
    #define ENTRY_ID_WIDTH 64
    #define ENTRY_ID_LOC        \
        PIN_L(ENTRY_ID_WIDTH,   \
         GEN_FILL               \
        )
    
    // The name panel/label
    #define ENTRY_NAME_WIDTH 160
    #define ENTRY_NAME_LOC                      \
        PIN_L(ENTRY_NAME_WIDTH,                 \
         SUB_BORDERMARGIN_L(ENTRY_ID_WIDTH,     \
          GEN_FILL                              \
        ))
    
    // The map name panel/label/button
    #define ENTRY_MAP_NAME_LOC                  \
        SUB_BORDERMARGIN_L(ENTRY_ID_WIDTH,      \
         SUB_BORDERMARGIN_L(ENTRY_NAME_WIDTH,   \
          GEN_FILL                              \
        ))
    
    MainWindow::Entry::Entry(rascUI::Theme * theme, MainWindow * mainWindow, uint64_t id, const std::string & playerName, const std::string & mapName) :
        rascUI::Container(rascUI::Location(ENTRY_OVERALL_LOC)),
        mainWindow(mainWindow),
        idPanel(rascUI::Location(ENTRY_ID_LOC)),
        idLabel(rascUI::Location(ENTRY_ID_LOC), std::to_string(id) + ':'),
        namePanel(rascUI::Location(ENTRY_NAME_LOC)),
        nameLabel(rascUI::Location(ENTRY_NAME_LOC), playerName),
        mapNameButton(rascUI::Location(ENTRY_MAP_NAME_LOC), mapName,
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    this->mainWindow->messageBox.setText(mapNameButton.getText());
                    this->mainWindow->messageBox.repaint();
                    this->mainWindow->setSendEnabled();
                }
            }
        ) {
        add(&idPanel);
        add(&idLabel);
        add(&namePanel);
        add(&nameLabel);
        add(&mapNameButton);
    }
    
    MainWindow::Entry::Entry(rascUI::Theme * theme, MainWindow * mainWindow, const std::string & statusStr) :
        rascUI::Container(rascUI::Location(ENTRY_OVERALL_LOC)),
        mainWindow(mainWindow),
        idPanel(),
        idLabel(rascUI::Location(), statusStr) {
        add(&idPanel);
        add(&idLabel);
    }
    
    MainWindow::Entry::~Entry(void) {
    }
    
    // =========================================================================
    
    MainWindow::MessageBox::MessageBox(rascUI::Location location, MainWindow * mainWindow) :
        rascUI::BasicTextEntryBox(location),
        mainWindow(mainWindow) {
    }
    
    MainWindow::MessageBox::~MessageBox(void) {
    }
    
    rascUI::NavigationAction MainWindow::MessageBox::onSelectionLockKeyPress(rascUI::NavigationAction defaultAction, int key, bool special) {
        rascUI::NavigationAction action = BasicTextEntryBox::onSelectionLockKeyPress(defaultAction, key, special);
        mainWindow->setSendEnabled();
        return action;
    }
    
    rascUI::NavigationAction MainWindow::MessageBox::onSelectionLockKeyRelease(rascUI::NavigationAction defaultAction, int key, bool special) {
        // If the user presses enter, send the message rather than breaking selection lock.
        if (defaultAction == rascUI::NavigationAction::enter && !Server::isEmptyOrWhitespace(getText())) {
            mainWindow->sendText();
            return rascUI::NavigationAction::noAction;
            
        // Otherwise use the superclass method.
        } else {
            return BasicTextEntryBox::onSelectionLockKeyRelease(defaultAction, key, special);
        }
    }
    
    void MainWindow::MessageBox::onChange(const std::string & str) {
        mainWindow->setSendEnabled();
    }
    
    // =========================================================================
    
    // All components fitted into this.
    #define OVERALL_LOC \
        GEN_FILL

    // The top back panel
    #define TOP_BACK_PANEL_LOC      \
        PIN_T(COUNT_OUTBORDER_Y(1), \
         OVERALL_LOC                \
        )

    // The top front panel
    #define TOP_FRONT_PANEL_LOC                 \
        SUB_BORDERMARGIN_R(EXIT_BUTTON_WIDTH,   \
         SUB_BORDER(                            \
          TOP_BACK_PANEL_LOC                    \
        ))

    // The title label
    #define TITLE_LABEL_WIDTH 100
    #define TITLE_LABEL_LOC         \
        PIN_L(TITLE_LABEL_WIDTH,    \
         TOP_FRONT_PANEL_LOC        \
        )

    // The name label
    #define NAME_LABEL_LOC                      \
        SUB_BORDERMARGIN_L(TITLE_LABEL_WIDTH,   \
         TOP_FRONT_PANEL_LOC                    \
        )

    // The exit button
    #define EXIT_BUTTON_WIDTH 48
    #define EXIT_BUTTON_LOC         \
        PIN_R(EXIT_BUTTON_WIDTH,    \
         SUB_BORDER(                \
          TOP_BACK_PANEL_LOC        \
        ))
        
    // The middle section
    #define MIDDLE_SECTION_LOC                      \
        SUB_BORDERMARGIN_Y(COUNT_ONEBORDER_Y(1),    \
         OVERALL_LOC                                \
        )

    // The scroll pane. Use a negative margin at the bottom to hide double border.
    #define SCROLL_PANE_LOC         \
        SUB_MARGIN_B(-UI_BORDER,    \
         SUB_BORDER_R(              \
          MIDDLE_SECTION_LOC        \
        ))

    // The bottom back panel
    #define BOTTOM_BACK_PANEL_LOC      \
        PIN_B(COUNT_OUTBORDER_Y(1),    \
         OVERALL_LOC                   \
        )

    // The message box
    #define MESSAGE_BOX_LOC                        \
        SUB_BORDERMARGIN_R(SEND_BUTTON_WIDTH,      \
         SUB_BORDER(                               \
          BOTTOM_BACK_PANEL_LOC                    \
        ))

    // The send button
    #define SEND_BUTTON_WIDTH 48
    #define SEND_BUTTON_LOC         \
        PIN_R(SEND_BUTTON_WIDTH,    \
         SUB_BORDER(                \
          BOTTOM_BACK_PANEL_LOC     \
        ))
    
    MainWindow::MainWindow(platform::Context * context, const rascUI::WindowConfig & config, rascUI::Theme * theme, GLfloat * scale, int * scaleExp) :
        ScaleWindow(context, config, scale, scaleExp),
        
        intf(this),
        topBackPanel(rascUI::Location(TOP_BACK_PANEL_LOC)),
        topFrontPanel(rascUI::Location(TOP_FRONT_PANEL_LOC)),
        titleLabel(rascUI::Location(TITLE_LABEL_LOC), "Your name:"),
        nameLabel(rascUI::Location(NAME_LABEL_LOC), "Bob"),
        exitButton(rascUI::Location(EXIT_BUTTON_LOC), "Exit",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    setMapped(false);
                }
            }
        ),
        middleBackPanel(rascUI::Location(MIDDLE_SECTION_LOC)),
        scrollPane(theme, rascUI::Location(SCROLL_PANE_LOC), true, true, false, true),
        bottomBackPanel(rascUI::Location(BOTTOM_BACK_PANEL_LOC)),
        messageBox(rascUI::Location(MESSAGE_BOX_LOC), this),
        sendButton(rascUI::Location(SEND_BUTTON_LOC), "Send",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) sendText();
            }
        ) {
        
        // Make send button inactive by default, since the default message is empty.
        sendButton.setActive(false);
        
        // Don't use animation in the scroll pane: this isn't OpenGL.
        scrollPane.contents.smoothMoveSpeed = 0.0f;
        
        add(&middleBackPanel);
        add(&scrollPane); // Scroll pane always first
        add(&topBackPanel);
        add(&topFrontPanel);
        add(&titleLabel);
        add(&nameLabel);
        add(&exitButton);
        add(&bottomBackPanel);
        add(&messageBox);
        add(&sendButton);
    }
    
    MainWindow::~MainWindow(void) {
    }
    
    void MainWindow::sendText(void) {
        if (canSend()) {
            intf.sendMapNameText(messageBox.getText());
            messageBox.setText(std::string());
            messageBox.repaint();
            setSendEnabled();
        }
    }
    
    void MainWindow::showStatusMessage(const std::string & msg) {
        entries.emplace_back(getTopLevel()->theme, this, msg);
        scrollPane.contents.add(&entries.back());
        scrollToBottomIfPossible();
        repaint(); // Repaint the whole window since we added a component.
    }
    
    void MainWindow::showMapName(uint64_t id, const std::string & playerName, const std::string & mapName) {
        entries.emplace_back(getTopLevel()->theme, this, id, playerName, mapName);
        scrollPane.contents.add(&entries.back());
        scrollToBottomIfPossible();
        repaint(); // Repaint the whole window since we added a component.
    }
    
    void MainWindow::setName(const std::string & name) {
        nameLabel.setText(name);
        topFrontPanel.repaint();
    }
    
    void MainWindow::setSendEnabled(void) {
        sendButton.setActive(canSend());
        sendButton.repaint();
    }
    
    bool MainWindow::canSend(void) {
        return !Server::isEmptyOrWhitespace(messageBox.getText());
    }
    
    void MainWindow::scrollToBottomIfPossible(void) {
        // Do nothing if the scrollpane has a height of zero (i.e: has not yet
        // been recalculated). If this is the case, then scrolling to the bottom
        // will result in scrolling *past* the bottom, since it thinks it has a
        // very small view size.
        if (scrollPane.contents.location.cache.height == 0.0f) return;
        
        rascUI::OffVal<size_t, GLfloat> maxSegmentPos = scrollPane.contents.getMaximumSegmentPos(),
                                        currentSegmentPos = scrollPane.contents.getSegmentPos();
        
        // Assuming ONE thing was just added, we should consider the user to be at the
        // bottom if BEFORE it was added, they were less than one segment from the bottom.
        // Since something was just added, consider that as less than two segments from the bottom.
        maxSegmentPos.zeroRoundSubtract(currentSegmentPos);
        if (maxSegmentPos.getBase() < 2) {
            scrollPane.contents.scrollSlowlyToEnd();
        }
    }
}
