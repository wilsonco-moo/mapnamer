/*
 * Server.h
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#ifndef MAPNAMER_SERVER_H_
#define MAPNAMER_SERVER_H_

#include <sockets/plus/simpleNetworkServer/SimpleNetworkServer.h>
#include <sockets/plus/networkinterface/NetworkInterface.h>
#include <unordered_set>
#include <cstdint>
#include <fstream>
#include <string>

namespace mapnamer {
    
    /**
     * Client to server message IDs.
     */
    class ClientToServer {
    public:
        enum {
            dummy,
            sendMapName,
            sendName
        };
    };
    
    /**
     * Server to client message IDs.
     */
    class ServerToClient {
    public:
        enum {
            dummy,
            status,
            mapNameResponse
        };
    };
    
    
    
    /**
     *
     */
    class Server : public simplenetwork::SimpleNetworkServer {
    public:
        /**
         * The maximum name length which clients should use.
         */
        const static size_t MAX_NAME_LENGTH;
        
        /**
         * Returns true if the string consists only of whitespace, or is empty.
         */
        static bool isEmptyOrWhitespace(const std::string & str);
        
        
    private:
        // This stops names from being repeated.
        std::unordered_set<std::string> previousNames;
        
        // Counts name IDs.
        uint64_t nameIdUpto;
        
        // The file we are logging names to.
        std::ofstream namesFile;
        
        class ServerIntf : public simplenetwork::NetworkInterface {
        private:
            std::string name;
            
        public:
            ServerIntf(void);
            virtual ~ServerIntf(void);
        protected:
            virtual void onConnected(void) override;
            virtual void receive(simplenetwork::Message & msg) override;
            virtual void onDisconnected(void) override;
        public:
            inline const std::string & getName(void) const {
                return name;
            }
        };
        
        
    public:
        Server(void);
        virtual ~Server(void);
    
    private:
        // Called by NetIntf when we receive a name. Either ends back a status message, or sends and saves the name.
        void onReceiveName(const std::string & playerName, const std::string & mapName, ServerIntf * intf);
    
        // Sends a welcome message to the client.
        void sendWelcomeMessage(ServerIntf * intf);
        
        // Broadcasts a status message to all players, except the one specified.
        // NULL can be specified to send to ALL.
        void broadcastStatusMessage(const std::string & message, ServerIntf * avoid);
    
    protected:
        virtual simplenetwork::NetworkInterface * createInterface(void) override;
    };
}

#endif
