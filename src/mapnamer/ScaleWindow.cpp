/*
 * ScaleWindow.cpp
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#include "ScaleWindow.h"

#include <cmath>
#include <iostream>

// For key symbols.
#ifdef _WIN32
    #include <windows.h>
#else
    #define XK_MISCELLANY
    #include <X11/keysymdef.h>
#endif

namespace mapnamer {

    ScaleWindow::ScaleWindow(platform::Context * context, const rascUI::WindowConfig & config, GLfloat * scale, int * scaleExp) :
        platform::Window(context, config),
        scale(scale),
        scaleExp(scaleExp) {
    }
    
    ScaleWindow::~ScaleWindow(void) {
    }
    
    void ScaleWindow::onUnusedKeyPress(int key, bool special) {
        int change = 0;
        #ifdef _WIN32
            if (special) {
                if (key == VK_F1) {
                    change = 1;
                } else if (key == VK_F2) {
                    change = -1;
                } else if (key == VK_F3) {
                    change = -*scaleExp;
                }
            }
        #else
            if (key == XK_F1) {
                change = 1;
            } else if (key == XK_F2) {
                change = -1;
            } else if (key == XK_F3) {
                change = -*scaleExp;
            }
        #endif
        if (change == 0) return;
        *scaleExp += change;
        *scale = std::pow(2.0f, ((GLfloat)*scaleExp) / 8.0f);
        getContext()->repaintEverything();
    }
}
