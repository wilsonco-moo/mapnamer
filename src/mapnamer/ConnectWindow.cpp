/*
 * ConnectWindow.cpp
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#include "ConnectWindow.h"

#include <threading/ThreadQueue.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "ClientIntf.h"
#include "Server.h"

#define LAYOUT_THEME theme

// The back panel
#define BACK_PANEL_LOC \
    GEN_FILL

// All components fitted into this.
#define OVERALL_LOC \
    SUB_BORDER(BACK_PANEL_LOC)

// The top section.
#define TOP_SECTION_HEIGHT \
    COUNT_OUTBORDER_Y(ROW_COUNT)
#define TOP_SECTION_LOC         \
    PIN_T(TOP_SECTION_HEIGHT,   \
     OVERALL_LOC                \
    )

// The location for rows.
#define ROW_COUNT 4
#define ROW_LOC(id)         \
    MOVE_BORDERNORMAL_Y(id, \
     SETNORMAL_Y(           \
      SUB_BORDER(           \
       TOP_SECTION_LOC      \
    )))

// The location for labels.
#define LABEL_WIDTH 128
#define LABEL_LOC(id)   \
    PIN_L(LABEL_WIDTH,  \
     ROW_LOC(id)        \
    )

// The location for boxes.
#define BOX_LOC(id)                 \
    SUB_BORDERMARGIN_L(LABEL_WIDTH, \
     ROW_LOC(id)                    \
    )

// The location for the bottom buttons.
#define BOTTOM_BUTTONS_COUNT 2
#define BOTTOM_BUTTONS_LOC(id)              \
    BORDERTABLE_X(id, BOTTOM_BUTTONS_COUNT, \
     BOX_LOC(ROW_COUNT - 1)                 \
    )

// The bottom section
#define BOTTOM_SECTION_LOC                  \
    SUB_BORDERMARGIN_T(TOP_SECTION_HEIGHT,  \
     OVERALL_LOC                            \
    )

// The location of the status label
#define STATUS_LABEL_LOC        \
    SETNORMAL_Y(                \
     PIN_B(UI_BHEIGHT * 0.5f,   \
      SPLIT_T(0.5f,             \
       BOTTOM_SECTION_LOC       \
    )))

namespace mapnamer {

    ConnectWindow::ConnectWindow(platform::Context * context, const rascUI::WindowConfig & config, ClientIntf * intf, rascUI::Theme * theme, GLfloat * scale, int * scaleExp) :
        ScaleWindow(context, config, scale, scaleExp),
        token(intf->threadQueue.createToken()),
        intf(intf),
        
        backPanel(rascUI::Location(BACK_PANEL_LOC)),
        topFrontPanel(rascUI::Location(TOP_SECTION_LOC)),
        addressLabel(rascUI::Location(LABEL_LOC(0)), "Server address:"),
        portLabel(rascUI::Location(LABEL_LOC(1)), "Server port:"),
        nameLabel(rascUI::Location(LABEL_LOC(2)), "Your name:"),
        addressBox(rascUI::Location(BOX_LOC(0)), "wilsonco.mooo.com",
            [this](const std::string & str) {
                setCanConnect();
            }
        ),
        portBox(rascUI::Location(BOX_LOC(1)), "64000",
            [this](const std::string & str) {
                portBox.setText(std::to_string((uint16_t)std::strtoul(portBox.getText().c_str(), NULL, 0)));
                setCanConnect();
            }
        ),
        nameBox(rascUI::Location(BOX_LOC(2)), std::string(),
            [this](const std::string & str) {
                if (nameBox.getText().length() > Server::MAX_NAME_LENGTH) {
                    nameBox.setText(nameBox.getText().substr(0, Server::MAX_NAME_LENGTH));
                }
                setCanConnect();
            }
        ),
        connectButton(rascUI::Location(BOTTOM_BUTTONS_LOC(0)), "Connect",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    // Get address, name and port.
                    std::string address = addressBox.getText(),
                                name = nameBox.getText();
                    uint16_t port = (uint16_t)std::strtoul(portBox.getText().c_str(), NULL, 0);
                    
                    // Set the network interface's name.
                    this->intf->setName(name);
                    
                    // Make components inactive and set status. Repaint window since we changed a lot.
                    // Don't disable the exit button -> it can still be pressed, the window will still close,
                    // but the token will be waited for until the timeout is up.
                    connectButton.setActive(false); addressBox.setActive(false); portBox.setActive(false); nameBox.setActive(false);
                    statusLabel.setText("Connecting to server...");
                    repaint();
                    
                    // Connect in a different thread, as to avoid blocking the UI.
                    // Use a timeout of 10 seconds.
                    this->intf->threadQueue.add(token, [this, address, port](void) {
                        bool status = this->intf->connectToServer(address, port, 10.0f);
                        
                        // While still on the separate thread, if connecting succeeded, send the name.
                        if (status) {
                            this->intf->sendName();
                        }
                        
                        getTopLevel()->beforeEvent->addFunction([this, status] {
                            // If the connection succeeded, unmap the connection window so we move the the next bit of the UI.
                            if (status) {
                                setMapped(false);
                                
                            // If the connection failed update the UI.
                            } else {
                                connectButton.setActive(true); addressBox.setActive(true); portBox.setActive(true); nameBox.setActive(true);
                                statusLabel.setText("Connection failed.");
                                repaint();
                            }
                        });
                    });
                }
            }
        ),
        exitButton(rascUI::Location(BOTTOM_BUTTONS_LOC(1)), "Exit",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) {
                    setMapped(false);
                }
            }
        ),
        bottomFrontPanel(rascUI::Location(BOTTOM_SECTION_LOC)),
        statusLabel(rascUI::Location(STATUS_LABEL_LOC), "Use F1, F2, F3 to change UI scale.") {
            
        // Set to initially inactive, until the user enters a name.
        connectButton.setActive(false);
            
        add(&backPanel);
        add(&topFrontPanel);
        add(&addressLabel);
        add(&portLabel);
        add(&nameLabel);
        add(&addressBox);
        add(&portBox);
        add(&nameBox);
        add(&connectButton);
        add(&exitButton);
        add(&bottomFrontPanel);
        add(&statusLabel);
    }
    
    ConnectWindow::~ConnectWindow(void) {
        intf->threadQueue.deleteToken(token);
    }
    
    void ConnectWindow::setCanConnect(void) {
        connectButton.setActive(!Server::isEmptyOrWhitespace(addressBox.getText()) &&
                                !Server::isEmptyOrWhitespace(nameBox.getText()) &&
                                !Server::isEmptyOrWhitespace(portBox.getText()));
    }
}
