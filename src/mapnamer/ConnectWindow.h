/*
 * ConnectWindow.h
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#ifndef MAPNAMER_CONNECTWINDOW_H_
#define MAPNAMER_CONNECTWINDOW_H_

#include "ScaleWindow.h"

#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>

namespace mapnamer {
    class ClientIntf;

    /**
     *
     */
    class ConnectWindow : public ScaleWindow {
    private:
        void * token;
        ClientIntf * intf;
    
        rascUI::BackPanel backPanel;
        rascUI::FrontPanel topFrontPanel;
        rascUI::Label addressLabel, portLabel, nameLabel;
        rascUI::BasicTextEntryBox addressBox, portBox, nameBox;
        rascUI::Button connectButton, exitButton;
        
        rascUI::FrontPanel bottomFrontPanel;
        rascUI::Label statusLabel;

    public:
        ConnectWindow(platform::Context * context, const rascUI::WindowConfig & config, ClientIntf * intf, rascUI::Theme * theme, GLfloat * scale, int * scaleExp);
        virtual ~ConnectWindow(void);
        
    private:
        // Sets the status of the connect button.
        void setCanConnect(void);
    };
}

#endif
