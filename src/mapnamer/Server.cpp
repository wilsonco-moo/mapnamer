/*
 * Server.cpp
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#include "Server.h"

#include <functional>
#include <algorithm>
#include <iostream>
#include <cctype>
#include <mutex>
#include <ios>

// A utility macro to convert a string to lowercase
#define MAKE_LOWERCASE(str) \
    std::transform((str).begin(), (str).end(), (str).begin(), [](unsigned char c){return std::tolower(c);});

namespace mapnamer {
    
    Server::ServerIntf::ServerIntf(void) :
        simplenetwork::NetworkInterface() {
    }
    Server::ServerIntf::~ServerIntf(void) {
        // NetworkInterface is a Killable subclass, this must be called first.
        killableDestroy();
    }
    
    void Server::ServerIntf::onConnected(void) {
        std::cout << "Client connected.\n";
    }
    
    void Server::ServerIntf::receive(simplenetwork::Message & msg) {
        switch(msg.getId()) {
            // Handle client name sending. Don't let clients change their name more than once.
            case ClientToServer::sendName: {
                if (name.empty()) {
                    std::string newName = msg.readString();
                    
                    // Make sure the name isn't too long.
                    if (newName.length() > MAX_NAME_LENGTH) {
                        std::cerr << "WARNING: Client sent name which was " << name.length() << " characters long. Clipping to " << MAX_NAME_LENGTH << " characters.\n";
                        newName = newName.substr(0, MAX_NAME_LENGTH);
                    }
                    
                    // Make sure the name isn't empty.
                    if (Server::isEmptyOrWhitespace(newName)) {
                        std::cerr << "WARNING: Client tried to set their name to something empty.\n";
                    
                    } else {
                        
                        // Set the name and print it.
                        name = newName;
                        std::cout << "Client announced their name: " << name << ".\n";
                    
                        // Send them back a welcome message.
                        ((Server *)getServer())->sendWelcomeMessage(this);
                        
                        // Broadcast a message to all other players saying we have joined.
                        ((Server *)getServer())->broadcastStatusMessage(std::string("Server: ") + name + " joined.", this);
                    }
                
                // Complain if the client tries to set their name twice.
                } else {
                    std::cerr << "WARNING: Client: " << name << " tried to change their name.\n";
                }
                break;
            }
            
            // Handle client name sending, only if we have a name already.
            case ClientToServer::sendMapName: {
                if (name.empty()) {
                    std::cerr << "WARNING: Client tried to send a map name, before setting their own name.\n";
                } else {
                    std::string mapName = msg.readString();
                    if (Server::isEmptyOrWhitespace(mapName)) {
                        std::cerr << "WARNING: Client " << name << " sent empty map name.\n";
                    } else {
                        ((Server *)getServer())->onReceiveName(name, mapName, this);
                    }
                }
                break;
            }
                
            default:
                std::cerr << "WARNING: Invalid message id: " << msg.getId() << ".\n";
                break;
        }
    }
    
    void Server::ServerIntf::onDisconnected(void) {
        if (name.empty()) {
            std::cout << "A client disconnected.\n";
        } else {
            std::cout << "Client disconnected: " << name << ".\n";
            
            // Broadcast a message to all other players saying we have left.
            ((Server *)getServer())->broadcastStatusMessage(std::string("Server: ") + name + " left.", this);
        }
    }
    
    
    // -------------------------------------------------------
    
    const size_t Server::MAX_NAME_LENGTH = 12;
    
    bool Server::isEmptyOrWhitespace(const std::string & str) {
        for (char character : str) {
            if (!std::isspace(character)) return false;
        }
        return true;
    }
    
    Server::Server(void) :
        previousNames(),
        nameIdUpto(),
        namesFile("namesList", std::ios::app) {
    }
    
    Server::~Server(void) {
        // SimpleNetworkServer is a Killable subclass, this must be called first.
        killableDestroy();
    }
    
    void Server::onReceiveName(const std::string & playerName, const std::string & mapName, ServerIntf * intf) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        
        std::string lowercaseName = mapName;
        MAKE_LOWERCASE(lowercaseName)
        
        // If the name didn't already exist, save and send it.
        if (previousNames.find(lowercaseName) == previousNames.end()) {
            previousNames.insert(lowercaseName);
            
            // Send the response message.
            simplenetwork::Message msg(ServerToClient::mapNameResponse);
            msg.write64(nameIdUpto);
            msg.writeString(intf->getName());
            msg.writeString(mapName);
            for (simplenetwork::NetworkInterface * sendTo : clientsRaw()) {
                sendTo->send(msg);
            }
            
            // Log the name in the server console.
            std::cout << "Player [" << playerName << "] sent map name " << nameIdUpto << ": [" << mapName << "].\n";
            nameIdUpto++;
            
            // Write the name to the names list file, (and flush it).
            namesFile << mapName << '\n';
            namesFile.flush();
            
        // If this name has been typed before, send back a status message complaining.
        } else {
            simplenetwork::Message msg(ServerToClient::status);
            msg.writeString(std::string("Name already exists: [") + mapName + "]");
            intf->send(msg);
            
            // Log the fact that the name was repeated.
            std::cout << "Player [" << playerName << "] REPEATED map name: [" << mapName << "].\n";
        }
    }
    
    void Server::sendWelcomeMessage(ServerIntf * intf) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        
        simplenetwork::Message msg(ServerToClient::status);
        msg.writeString(std::string("Server: Welcome ") + intf->getName() + ", there are " + std::to_string(nameIdUpto) + " map names so far.");
        intf->send(msg);
        
        // Count how many other players have names.
        size_t otherNonEmpty = 0;
        for (simplenetwork::NetworkInterface * other : clientsRaw()) {
            if (other != intf && !((ServerIntf *)other)->getName().empty()) {
                otherNonEmpty++;
            }
        }
        
        // If more than one other player has a name, print a list of names.
        if (otherNonEmpty > 0) {
            msg.clear();
            msg.setId(ServerToClient::status);
            msg.writeString("Other currently connected people are listed below:");
            intf->send(msg);
            
            for (simplenetwork::NetworkInterface * other : clientsRaw()) {
                if (other != intf && !((ServerIntf *)other)->getName().empty()) {
                    msg.clear();
                    msg.setId(ServerToClient::status);
                    msg.writeString(((ServerIntf *)other)->getName());
                    intf->send(msg);
                }
            } 
        }
    }
    
    void Server::broadcastStatusMessage(const std::string & message, ServerIntf * avoid) {
        std::lock_guard<std::recursive_mutex> lock(killableMutex);
        simplenetwork::Message msg(ServerToClient::status);
        msg.writeString(message);
        for (simplenetwork::NetworkInterface * sendTo : clientsRaw()) {
            if (sendTo != avoid) {
                sendTo->send(msg);
            }
        }
    }
    
    simplenetwork::NetworkInterface * Server::createInterface(void) {
        return new ServerIntf();
    }
}
