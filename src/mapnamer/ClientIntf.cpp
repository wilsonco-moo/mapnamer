/*
 * ClientIntf.cpp
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#include "ClientIntf.h"

#include <sockets/plus/message/Message.h>
#include <iostream>

#include "MainWindow.h"
#include "Server.h"

namespace mapnamer {

    ClientIntf::ClientIntf(MainWindow * mainWindow) :
        simplenetwork::NetworkInterface(),
        name(),
        mainWindow(mainWindow),
        threadQueue(1) {
    }
    
    ClientIntf::~ClientIntf(void) {
    }
    
    void ClientIntf::onConnected(void) {
    }
    
    void ClientIntf::receive(simplenetwork::Message & msg) {
        switch(msg.getId()) {
            // Show status messages in the main window.
            case ServerToClient::status: {
                std::string statusMessage = msg.readString();
                mainWindow->getContext()->beforeEvent.addFunction([this, statusMessage](void) {
                    mainWindow->showStatusMessage(statusMessage);
                });
                break;
            }
            
            case ServerToClient::mapNameResponse: {
                uint64_t id = msg.read64();
                std::string playerName = msg.readString();
                std::string mapName = msg.readString();
                mainWindow->getContext()->beforeEvent.addFunction([this, id, playerName, mapName](void) {
                    mainWindow->showMapName(id, playerName, mapName);
                });
                break;
            }
            
            default:
                std::cerr << "WARNING: Invalid message id: " << msg.getId() << ".\n";
                break;
        }
    }
    
    void ClientIntf::onDisconnected(void) {
        // Notify the client if the server closes.
        mainWindow->getContext()->beforeEvent.addFunction([this](void) {
            mainWindow->showStatusMessage("Notice: disconnected from server.");
        });
    }
    
    void ClientIntf::setName(const std::string & newName) {
        name = newName;
        mainWindow->setName(newName);
    }
    
    void ClientIntf::sendName(void) {
        simplenetwork::Message msg(ClientToServer::sendName);
        msg.writeString(name);
        send(msg);
    }
    
    void ClientIntf::sendMapNameText(const std::string & mapName) {
        simplenetwork::Message msg(ClientToServer::sendMapName);
        msg.writeString(mapName);
        send(msg);
    }
}
