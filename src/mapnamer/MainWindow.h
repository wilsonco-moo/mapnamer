/*
 * MainWindow.h
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#ifndef MAPNAMER_MAINWINDOW_H_
#define MAPNAMER_MAINWINDOW_H_

#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/Container.h>

#include "ScaleWindow.h"
#include "ClientIntf.h"

namespace mapnamer {
    class ClientIntf;

    /**
     *
     */
    class MainWindow : public ScaleWindow {
    private:
        class Entry : public rascUI::Container {
        private:
            MainWindow * mainWindow;
            rascUI::FrontPanel idPanel;
            rascUI::Label idLabel;
            rascUI::FrontPanel namePanel;
            rascUI::Label nameLabel;
            rascUI::Button mapNameButton;
        
        public:
            // Creates a map name entry.
            Entry(rascUI::Theme * theme, MainWindow * mainWindow, uint64_t id, const std::string & playerName, const std::string & mapName);
            
            // Creates a status message entry.
            Entry(rascUI::Theme * theme, MainWindow * mainWindow, const std::string & statusStr);
            
            virtual ~Entry(void);
        };
        
        // ---------------------------------------------------------------------
    
        class MessageBox : public rascUI::BasicTextEntryBox {
        private:
            MainWindow * mainWindow;
            
        public:
            MessageBox(rascUI::Location location, MainWindow * mainWindow);
            virtual ~MessageBox(void);
            
        protected:
            // Override the key press enter behaviour so we always update the status of the send button.
            virtual rascUI::NavigationAction onSelectionLockKeyPress(rascUI::NavigationAction defaultAction, int key, bool special) override;
        
            // Override key release "enter" behaviour to send message and clear instead of breaking keyboard selection lock.
            virtual rascUI::NavigationAction onSelectionLockKeyRelease(rascUI::NavigationAction defaultAction, int key, bool special) override;
            
            // Clip length on change.
            virtual void onChange(const std::string & str) override;
        };
    
        // ---------------------------------------------------------------------
    
        ClientIntf intf;
    
        rascUI::BackPanel topBackPanel;
        rascUI::FrontPanel topFrontPanel;
        rascUI::Label titleLabel, nameLabel;
        rascUI::Button exitButton;
        
        rascUI::BackPanel middleBackPanel;
        rascUI::ScrollPane scrollPane;
        
        rascUI::BackPanel bottomBackPanel;
        MessageBox messageBox;
        rascUI::Button sendButton;
        
        std::list<Entry> entries;
    
    public:
        MainWindow(platform::Context * context, const rascUI::WindowConfig & config, rascUI::Theme * theme, GLfloat * scale, int * scaleExp);
        virtual ~MainWindow(void);
        
    private:
        // Sends the map name.
        void sendText(void);
        
        // Sets the enabled status of the send button.
        void setSendEnabled(void);
        
        // Returns true if we can send the map name (i.e: it is not empty).
        bool canSend(void);
        
        // Scrolls the scrollpane to the bottom, but only if the user is already scrolled to the bottom.
        // This should be called after each time something is added to the scroll pane.
        void scrollToBottomIfPossible(void);
        
    public:
        inline ClientIntf * getClientIntf(void) {
            return &intf;
        }
        
        /**
         * Shows a status message. This must be called from the UI thread.
         */
        void showStatusMessage(const std::string & statusStr);
        
        /**
         * Shows a map message. This must be called from the UI thread.
         */
        void showMapName(uint64_t id, const std::string & playerName, const std::string & mapName);
        
        /**
         * Sets the player name shown at the top.
         */
        void setName(const std::string & name);
    };
}

#endif
