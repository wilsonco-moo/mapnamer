/*
 * ClientIntf.h
 *
 *  Created on: 2 Jun 2020
 *      Author: wilson
 */

#ifndef MAPNAMER_CLIENTINTF_H_
#define MAPNAMER_CLIENTINTF_H_

#include <sockets/plus/networkinterface/NetworkInterface.h>
#include <threading/ThreadQueue.h>

namespace mapnamer {
    class MainWindow;

    /**
     *
     */
    class ClientIntf : public simplenetwork::NetworkInterface {
    private:
        std::string name;
        MainWindow * mainWindow;

    public:
        // Used for running background jobs.
        threading::ThreadQueue threadQueue;
    
    
        ClientIntf(MainWindow * mainWindow);
        virtual ~ClientIntf(void);
        
    protected:
        virtual void onConnected(void) override;
        virtual void receive(simplenetwork::Message & msg) override;
        virtual void onDisconnected(void) override;
        
    public:
        /**
         * Sets our name.
         */
        void setName(const std::string & name);
        
        /**
         * Sends our name to the server, must only be called once.
         */
        void sendName(void);
        
        /**
         * Sends map name text to the server.
         */
        void sendMapNameText(const std::string & mapName);
        
        /**
         * Allows access to our name.
         */
        inline const std::string & getName(void) const {
            return name;
        }
    };
}

#endif
